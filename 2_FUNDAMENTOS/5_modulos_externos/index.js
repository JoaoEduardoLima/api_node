// no terminal foi feito os passos:
// - npm init
// - npm install minimist
// - node index.js --nome=João --profissao=Programador

const minimist = require("minimist")

const args = minimist(process.argv.slice(2))

console.log(args)

const nome = args["nome"]
const profissao = args["profissao"]

console.log(`O nome é ${nome} e a profissão é ${profissao}`)