// no terminal foi feito os passos:
// - npm init
// - npm install minimist
// - node index.js --a=10 --b=15

const minimist = require("minimist")

const soma = require('./soma').soma
const args = minimist(process.argv.slice(2))

const a = parseInt(args['a'])
const b = parseInt(args['b'])

soma(a, b)